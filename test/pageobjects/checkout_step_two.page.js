import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class CheckoutStepTwoPage extends Page {
    /**
     * define selectors using getter methods
     */
    get itemLabel() {
        return $('.inventory_item_name')
    }

    /**
     * define selectors using getter methods
     */
    get submitButton() {
        return $('#finish')
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('checkout-step-two.html');
    }
}

export default new CheckoutStepTwoPage();
