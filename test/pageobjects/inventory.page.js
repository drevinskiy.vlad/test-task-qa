import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class InventoryPage extends Page {

    /**
     * define selectors using getter methods
     */
    get inventoryList() {
        return $('.inventory_list')
    }

    /**
     * define selectors using getter methods
     */
    get menuButton() {
        return $('#react-burger-menu-btn')
    }

    /**
     * define selectors using getter methods
     */
    get logoutButton() {
        return $('#logout_sidebar_link')
    }

    /**
     * define selectors using getter methods
     */
    get addToCartButton() {
        return $('button[class*="btn_inventory"][id*="add-to-cart"]')
    }

    /**
     * define selectors using getter methods
     */
    get removeFromCartButton() {
        return $('button[class*="btn_inventory"][id*="remove"]')
    }

    /**
     * define selectors using getter methods
     */
    get cartButton() {
        return $('a.shopping_cart_link')
    }

    /**
     * define selectors using getter methods
     */
    get itemLabel() {
        return $('#inventory_container .inventory_item:first-of-type .inventory_item_name')
    }

    /**
     * define selectors using getter methods
     */
    get itemLink() {
        return $('#inventory_container .inventory_item:first-of-type a')
    }

    /**
     * define selectors using getter methods
     */
    get itemDetail() {
        return $('.inventory_details_container')
    }

    /**
     * define selectors using getter methods
     */
    get shoppingCartBadge() {
        return $('.shopping_cart_badge')
    }

    /**
     * define selectors using getter methods
     */
    get sortingSelect() {
        return $('.product_sort_container')
    }

    /**
     * define selectors using getter methods
     */
    get sortByAZ() {
        return $('.product_sort_container [value=az]')
    }

    /**
     * define selectors using getter methods
     */
    get sortByZA() {
        return $('.product_sort_container [value=za]')
    }

    /**
     * define selectors using getter methods
     */
    get sortByLOHI() {
        return $('.product_sort_container [value=lohi]')
    }

    /**
     * define selectors using getter methods
     */
    get sortByHILO() {
        return $('.product_sort_container [value=hilo]')
    }

    /**
     * define selectors using getter methods
     */
    get twitterLinkButton() {
        return $('a[href*="twitter"]')
    }

    /**
     * define selectors using getter methods
     */
    get facebookLinkButton() {
        return $('a[href*="facebook"]')
    }

    /**
     * define selectors using getter methods
     */
    get linkedinLinkButton() {
        return $('a[href*="linkedin"]')
    }

    /**
     * open source page and enter login and password to fields
     */
    async initCartItemQuantity() {
        if (await this.shoppingCartBadge.isExisting())
            return parseInt(await this.shoppingCartBadge.getText())
        else
            return 0
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('inventory.html');
    }
}

export default new InventoryPage();
