import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class CheckoutCompletePage extends Page {
    /**
     * define selectors using getter methods
     */
    get completeText() {
        return $('h2.complete-header')
    }

    /**
     * define selectors using getter methods
     */
    get backHomeButton() {
        return $('#back-to-products')
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('checkout-complete');
    }
}

export default new CheckoutCompletePage();
