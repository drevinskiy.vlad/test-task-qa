import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class CartPage extends Page {
    /**
     * define selectors using getter methods
     */
    get lastItemLabel() {
        return $('.inventory_item_name:last-of-type')
    }

    /**
     * define selectors using getter methods
     */
    get checkoutButton() {
        return $('#checkout')
    }

    /**
     * define selectors using getter methods
     */
    get errorMessage() {
        return $('#error')
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('cart.html');
    }
}

export default new CartPage();
