import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputUsername() {
        return $('#user-name');
    }

    /**
     * define selectors using getter methods
     */
    get inputPassword() {
        return $('#password');
    }

    /**
     * define selectors using getter methods
     */
    get inputUsernameCrossSign() {
        return $('.form_group #user-name+svg.error_icon')
    }

    /**
     * define selectors using getter methods
     */
    get inputPasswordCrossSign() {
        return $('.form_group #password+svg.error_icon')
    }

    /**
     * define selectors using getter methods
     */
    get errorMessage() {
        return $('.error-message-container.error h3')
    }

    /**
     * define selectors using getter methods
     */
    get btnSubmit() {
        return $('input[type="submit"]')
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login(username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.click();
    }

    async openAndLogin() {
        await this.open()
        await this.login('standard_user', 'secret_sauce')
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('');
    }
}

export default new LoginPage();
