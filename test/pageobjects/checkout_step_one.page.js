import {$} from '@wdio/globals'
import Page from './page.js';

/**
 * sub-page containing specific selectors and methods for a specific page
 */
class CheckoutStepOnePage extends Page {
    /**
     * define selectors using getter methods
     */
    get checkoutForm() {
        return $('#checkout_info_container')
    }

    /**
     * define selectors using getter methods
     */
    get firstNameInput() {
        return $('#first-name')
    }

    /**
     * define selectors using getter methods
     */
    get lastNameInput() {
        return $('#last-name')
    }

    /**
     * define selectors using getter methods
     */
    get postalCodeInput() {
        return $('#postal-code')
    }

    /**
     * define selectors using getter methods
     */
    get submitButton() {
        return $('#continue')
    }

    /**
     * define selectors using getter methods
     */
    get errorMessage() {
        return $('.error h3')
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to enter all data to form
     * @param firstName first name of user
     * @param lastName last name of user
     * @param postalCode postal code of user
     */
    async enterData(firstName, lastName, postalCode) {
        await this.firstNameInput.setValue(firstName);
        await this.lastNameInput.setValue(lastName);
        await this.postalCodeInput.setValue(postalCode);
        await this.submitButton.click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('checkout-step-one.html');
    }
}

export default new CheckoutStepOnePage();
