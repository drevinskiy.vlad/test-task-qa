import InventoryPage from "../pageobjects/inventory.page.js";
import CartPage from "../pageobjects/cart.page.js";
import LoginPage from "../pageobjects/login.page.js";

/**
 * initialize cart items quantity
 */


describe('cart', () => {
    it('Remove from cart', async () => {
        await LoginPage.openAndLogin()

        let itemLabel = InventoryPage.itemLabel.getText()//to check if item in cart identical as selected item
        let cartItemQuantity = await InventoryPage.initCartItemQuantity()//to check if number of items in cart are increased

        await InventoryPage.addToCartButton.click()
        await expect(await InventoryPage.initCartItemQuantity()).toEqual(cartItemQuantity + 1)

        await InventoryPage.removeFromCartButton.click()
        await expect(await InventoryPage.initCartItemQuantity()).toEqual(cartItemQuantity)

        await InventoryPage.cartButton.click()
        if (await CartPage.lastItemLabel.isExisting())
            await expect(await CartPage.lastItemLabel).not.toHaveText(await itemLabel)
    })
    it('Saving the card after logout ', async () => {
        await LoginPage.openAndLogin()

        let itemLabel = InventoryPage.itemLabel.getText()//to check if item in cart identical as selected item
        let cartItemQuantity = await InventoryPage.initCartItemQuantity()//to check if number of items in cart are increased

        await InventoryPage.addToCartButton.click()
        await expect(await InventoryPage.initCartItemQuantity()).toEqual(cartItemQuantity + 1)

        await InventoryPage.menuButton.click()
        await InventoryPage.logoutButton.click()
        await LoginPage.login('standard_user', 'secret_sauce')

        await InventoryPage.cartButton.click()
        await expect(await CartPage.lastItemLabel).toHaveText(await itemLabel)
    })
})