import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";

describe('Footer', () => {
    it('footer links', async () => {
        await LoginPage.openAndLogin()

        await InventoryPage.twitterLinkButton.click()
        await browser.switchWindow('twitter.com')
        await expect(await browser.getUrl()).toEqual('https://twitter.com/saucelabs')
        await browser.switchWindow('saucedemo.com')


        await InventoryPage.facebookLinkButton.click()
        await browser.switchWindow('facebook.com')
        await expect(await browser.getUrl()).toEqual('https://www.facebook.com/saucelabs')
        await browser.switchWindow('saucedemo.com')


        await InventoryPage.linkedinLinkButton.click()
        await browser.switchWindow('linkedin.com')
        await expect(await browser.getUrl()).toEqual('https://www.linkedin.com/company/sauce-labs/')
        await browser.switchWindow('saucedemo.com')
    })
})