import InventoryPage from "../pageobjects/inventory.page.js";
import inventoryPage from "../pageobjects/inventory.page.js";
import LoginPage from "../pageobjects/login.page.js";

describe('Products', () => {
    it('Sorting', async () => {
        await LoginPage.openAndLogin()

        await InventoryPage.sortingSelect.click()
        await InventoryPage.sortByZA.click()
        await expect(inventoryPage.itemLabel).toHaveText('Test.allTheThings() T-Shirt (Red)')


        await InventoryPage.sortingSelect.click()
        await InventoryPage.sortByLOHI.click()
        await expect(inventoryPage.itemLabel).toHaveText('Sauce Labs Onesie')


        await InventoryPage.sortingSelect.click()
        await InventoryPage.sortByHILO.click()
        await expect(inventoryPage.itemLabel).toHaveText('Sauce Labs Fleece Jacket')

        await InventoryPage.sortingSelect.click()
        await InventoryPage.sortByAZ.click()
        await expect(inventoryPage.itemLabel).toHaveText('Sauce Labs Backpack')
    })

    it('Product details', async () => {
        await LoginPage.openAndLogin()

        await inventoryPage.itemLink.click()
        await expect(InventoryPage.itemDetail).toBeDisplayed()
    })
})