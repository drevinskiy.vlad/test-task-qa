import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";
import CartPage from "../pageobjects/cart.page.js";
import CheckoutStepOnePage from "../pageobjects/checkout_step_one.page.js";
import CheckoutStepTwoPage from "../pageobjects/checkout_step_two.page.js";
import CheckoutCompletePage from "../pageobjects/checkout_complete.page.js";

describe('Checkout', () => {
    async function goToCheckoutStepOne() {
        await LoginPage.openAndLogin()

        let itemLabel = InventoryPage.itemLabel.getText()//to check if item in cart identical as selected item
        let cartItemQuantity = await InventoryPage.initCartItemQuantity()//to check if number of items in cart are increased

        await InventoryPage.addToCartButton.click()
        await expect(await InventoryPage.initCartItemQuantity()).toEqual(cartItemQuantity + 1)

        await InventoryPage.cartButton.click()
        await expect(await CartPage.lastItemLabel).toHaveText(await itemLabel)

        await CartPage.checkoutButton.click()
        await expect(CheckoutStepOnePage.checkoutForm).toBeDisplayed()
    }

    it('valid checkout', async () => {
        let itemLabel = InventoryPage.itemLabel.getText()//to check if item in cart identical as selected item

        await goToCheckoutStepOne()
        await CheckoutStepOnePage.enterData('firstName', 'lastName', '21000')

        await expect(CheckoutStepTwoPage.itemLabel).toHaveText(await itemLabel)
        await CheckoutStepTwoPage.submitButton.click()

        await expect(CheckoutCompletePage.completeText).toHaveText('Thank you for your order!')
        await CheckoutCompletePage.backHomeButton.click()

        await expect(InventoryPage.inventoryList).toBeDisplayed()
        await expect(InventoryPage.shoppingCartBadge).not.toExist()
    })

    //bug detected: error message was not displayed
    it.skip('checkout without products', async () => {
        await LoginPage.openAndLogin()

        await InventoryPage.cartButton.click()

        while (await InventoryPage.removeFromCartButton.isExisting())
            await InventoryPage.removeFromCartButton.click()

        await CartPage.checkoutButton.click()

        await expect(CartPage.errorMessage).toHaveText('Cart is Empty')
    })

    it('checkout without first name', async () => {
        await goToCheckoutStepOne()

        await CheckoutStepOnePage.enterData('', 'lastName', '21000')
        await expect(CheckoutStepOnePage.errorMessage).toHaveTextContaining(
            'Error: First Name is required')
    })

    it('checkout without last name', async () => {
        await goToCheckoutStepOne()

        await CheckoutStepOnePage.enterData('firstName', '', '21000')
        await expect(CheckoutStepOnePage.errorMessage).toHaveTextContaining(
            'Error: Last Name is required')
    })

    it('checkout without postal code', async () => {
        await goToCheckoutStepOne()

        await CheckoutStepOnePage.enterData('firstName', 'lastName', '')
        await expect(CheckoutStepOnePage.errorMessage).toHaveTextContaining(
            'Error: Postal Code is required')
    })
})