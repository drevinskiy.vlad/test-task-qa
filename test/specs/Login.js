import LoginPage from "../pageobjects/login.page.js";
import InventoryPage from "../pageobjects/inventory.page.js";

describe('Login', () => {

    /**
     * check all forms and messages if user entered invalid credentials
     */
    async function invalidCredentialsCheck() {
        await expect(LoginPage.inputUsername).toHaveStyle({'border-bottom-color': 'rgba(226,35,26,1)'})
        await expect(LoginPage.inputPassword).toHaveStyle({'border-bottom-color': 'rgba(226,35,26,1)'})

        await expect(LoginPage.inputUsernameCrossSign).toBeDisplayed()
        await expect(LoginPage.inputPasswordCrossSign).toBeDisplayed()

        await expect(LoginPage.errorMessage).toBeDisplayed()
    }

    it('Valid login', async () => {
        await LoginPage.openAndLogin()

        await expect(InventoryPage.inventoryList).toBeDisplayed()
    })

    it('Login with invalid password', async () => {
        await LoginPage.open()
        await LoginPage.login('standard_user', 'invalid_password')

        await invalidCredentialsCheck()
        await expect(LoginPage.errorMessage).toHaveTextContaining(
            'Epic sadface: Username and password do not match any user in this service')
    })

    it('Login with invalid login', async () => {
        await LoginPage.open()
        await LoginPage.login('invalid_login', 'secret_sauce')

        await invalidCredentialsCheck()
        await expect(LoginPage.errorMessage).toHaveTextContaining(
            'Epic sadface: Username and password do not match any user in this service')
    })

    it('Logout', async () => {
        await LoginPage.openAndLogin()

        await InventoryPage.menuButton.click()
        await InventoryPage.logoutButton.click()

        await expect(LoginPage.inputUsername).toHaveValue('')
        await expect(LoginPage.inputPassword).toHaveValue('')
    })

    it('Login with invalid login and password', async () => {
        await LoginPage.open()
        await LoginPage.login('invalid_login', 'invalid_password')

        await invalidCredentialsCheck()
        await expect(LoginPage.errorMessage).toHaveTextContaining(
            'Epic sadface: Username and password do not match any user in this service')
    })

    it('Login as locked out user', async () => {
        await LoginPage.open()
        await LoginPage.login('locked_out_user', 'secret_sauce')

        await invalidCredentialsCheck()
        await expect(LoginPage.errorMessage).toHaveTextContaining(
            'Epic sadface: Sorry, this user has been locked out.')
    })
})