# Automation QA Test Task

The task is to automatizate [some testcases](https://docs.google.com/spreadsheets/d/1Ck5PQNAJlEt9W2Z_5YcjUfIXFBbUyvjqFU1Fk9epGHQ/edit#gid=1505511202) using WebDriverIO framework

## Instructions for installing and running on a local server

- Instal **node.js** from [official website](https://nodejs.org/en/download)
- Download the project (clone via [git](https://gitlab.com/drevinskiy.vlad/test-task-qa) or download project and unzip).
- Open the Command Prompt (console) from the project folder or navigate in the Command Prompt to the project folder where contains package.json.
- Execute in the Command Prompt the command: _npm run wdio_


